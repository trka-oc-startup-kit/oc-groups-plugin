<?php namespace trka\Groups\Models;

use Model;
use Carbon\Carbon;
use October\Rain\Database\Traits\Sluggable;

//use Markdown;
//use RainLab\Blog\Classes\TagProcessor;
use Rainlab\Blog\Models\Post as RainlabBlogPost;
use RainLab\User\Models\User;
use trka\Groups\Classes\MemberRoles;

/**
 * Model
 */
class Group extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'trka_groups_groups';

    //--------------------------------------------- Relations
    public $belongsTo = [
        'forum_channel' => [
            'RainLab\Forum\Models\Channel'
        ]
    ];

    public $attachOne = [
        'group_image' => [
            'System\Models\File'
        ]
    ];

    public $belongsToMany = [
        'users' => [
            'Rainlab\User\Models\User',
            'table' => 'trka_groups_group_user',
            'key' => 'group_id',
            'otherKey' => 'user_id',
            'pivot' => [
                'moderator', 'role'
            ]
        ],
    ];

    protected $jsonable = [
        'notices',
        'sidebar_slots'
    ];

    //--------------------------------------------- Model Methods

    /**
     * Get users seen in the last sinceHours hours
     * @param int $sinceHours
     * @return mixed
     */
    public function usersActiveSince($sinceHours = 24)
    {
        $benchmark = Carbon::now()->subHours($sinceHours)->toDateTimeString();
        return $this->users->where('last_seen', '>', $benchmark)->sortByDesc('last_seen');
    }

    /**
     * Get all users, sorted by most recent
     * @return mixed
     */
    public function recentUsers()
    {
        return $this->users->sortByDesc('last_seen');
    }

    /**
     * Get topics posted to in the last sinceHours hours
     * @param int $sinceHours
     * @return mixed
     */
    public function topicsPostedSince($sinceHours = 24)
    {
        if (!$this->forum_channel) {
            return false;
        }
        $benchmark = Carbon::now()->subHours($sinceHours)->toDateTimeString();
        return $this->forum_channel->topics->where('last_post_at', '>', $benchmark)->sortByDesc('last_post_at');
    }

    /**
     * Get all topics, sorted by most recent activity
     * @return bool
     */
    public function recentTopics()
    {
        if (!$this->forum_channel) {
            return false;
        }

        return $this->forum_channel->topics->sortByDesc('last_post_at');
    }

    //--------------------------------------------- Scopes

    /**
     * Get group users with admin role
     * @return mixed
     */
    public function administrators()
    {
        return $this->usersByRole(MemberRoles::USER_ROLES['USER_ROLE_ADMINISTRATOR']['role']);
    }

    /**
     * get group users with moderator role
     * @return mixed
     */
    public function moderators()
    {
        return $this->usersByRole(MemberRoles::USER_ROLES['USER_ROLE_MODERATOR']['role']);
    }

    public function unprivileged()
    {
        return $this->usersByRole(MemberRoles::USER_ROLES['USER_ROLE_MEMBER']['role']);
    }

    /**
     * Internal fn for listing users having a given role string
     * @param $role
     * @return mixed
     */
    public function usersByRole($role)
    {
        return $this->users()
            ->wherePivot('role', $role)
            ->get();
    }

    //--------------------------------------------- Lifecycle hooks
    public function beforeSave()
    {
        $this->homepage_content_html = self::formatHtml($this->homepage_content);
    }

    //--------------------------------------------- Model Methods
    public static function formatHtml($input, $preview = false)
    {
        return RainlabBlogPost::formatHtml($input, $preview);
    }

    public function modifyRole(User $user, $role, $action)
    {
        switch ($action) {
            case 'add':
                return $user->usergroups()->attach($this, ['role' => $role]);
                break;
            case 'remove':
                return \Db::table('trka_groups_group_user')
                    ->where('user_id', $user->id)
                    ->where('group_id', $this->id)
                    ->where('role', $role)
                    ->delete();
                break;
        }
    }
}
