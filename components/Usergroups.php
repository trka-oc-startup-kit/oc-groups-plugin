<?php namespace trka\Groups\Components;

use Cms\Classes\ComponentBase;
use trka\Groups\Models\Group;

class Usergroups extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Usergroups',
            'description' => 'display all usergroups'
        ];
    }

    public function defineProperties()
    {
        return [
            'group_type' => [
                'title' => 'Type',
                'description' => 'Filter groups by type'
            ]
        ];
    }

    public function onRun()
    {
        $groupType = $this->property('group_type');
        $groupsQuery = Group::orderBy('name', 'asc');

        if ($groupType) {
            $groupsQuery = $groupsQuery->where('group_type', $groupType);
        }
        $groups = $groupsQuery->get();
        $this->page['groups'] = $groups;
    }
}