<?php namespace trka\Groups\Components;

use Auth;
use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use October\Rain\Support\Collection;
use RainLab\User\Models\User;
use trka\Groups\Classes\MemberRoles;
use trka\Groups\Models\Group;

class Usergroup extends ComponentBase
{
    public $usergroup;

    public function componentDetails()
    {
        return [
            'name' => 'Usergroup',
            'description' => 'display single usergroup'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'Slug',
                'description' => 'Display group by slug'
            ]
        ];
    }

    public function onRun()
    {
        $this->loadGroup();
    }

    /**
     * Facade for accessing MemberRoles constants in frontend.
     * @return array
     */
    public function getRoleOptions()
    {
        return MemberRoles::USER_ROLES;
    }

    //-------------------------------------------------- Ajax Handlers

    /**
     * Unprivileged action: allows user to self-assign member role
     * @return bool
     */
    public function onAddSelf()
    {
        $this->loadGroup();
        $user = \Auth::getUser();
        if (null === $user) {
            return false;
        }
        $this->usergroup->modifyRole($user, MemberRoles::USER_ROLES['USER_ROLE_MEMBER']['role'], 'add');
        return \Redirect::refresh();
    }

    /**
     * Allows user to remove own user-group relationship
     * @return bool
     */
    public function onRemoveSelf()
    {
        $this->loadGroup();
        $user = \Auth::getUser();
        if (null === $user) {
            return false;
        }
        $this->usergroup->modifyRole($user, MemberRoles::USER_ROLES['USER_ROLE_MEMBER']['role'], 'remove');
        return \Redirect::refresh();
    }

    /**
     * More privileged modify action. Allows arbitrarily mutating user-group roles
     * Should only be available to admin users
     */
    public function onModifyRoles()
    {
        $this->loadGroup();
        // @todo: this allows user-input to determine relation. potentially exploitable: if someone changes the ids over-the-wire, there's a risk of privilege escalation.
        //-- collect inputs: userid, role, action
        $userid = input('userid');
        $role = input('role');
        $action = input('action');
        //-- collect models
        $user = User::where('id', $userid)->first();
        if (null === $user) {
            return false;
        }

        $this->usergroup->modifyRole($user, $role, $action);

        return \Redirect::refresh();
    }

    //-------------------------------------------------- Internals

    protected function loadGroup($bySlug = null)
    {
        if (null === $bySlug) {
            $bySlug = $this->property('slug');
        }
        $group = Group::where('slug', $bySlug)->first();
        $this->usergroup = $this->page['group'] = $group;
    }
}