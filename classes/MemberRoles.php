<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 8/4/18
 * Time: 6:32 AM
 */

namespace trka\Groups\Classes;


class MemberRoles
{
    CONST USER_ROLES = [
        'USER_ROLE_ADMINISTRATOR' => [
            'role' => 'trka:groups:group_administrator',
            'label' => 'administrator'
        ],
        'USER_ROLE_MODERATOR' => [
            'role' => 'trka:groups:group_moderator',
            'label' => 'moderator'
        ],
        'USER_ROLE_MEMBER' => [
            'role' => 'trka:groups:group_member',
            'label' => 'member'
        ]
    ];
}