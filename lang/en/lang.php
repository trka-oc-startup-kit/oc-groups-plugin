<?php return [
    'plugin' => [
        'name' => 'Groups',
        'description' => 'Supports user groups',
    ],
    'group' => [
        'name' => 'Group Name',
        'slug' => 'Slug',
        'description' => 'Group Description',
        'channels' => 'Channels',
        'tabs' => [
            'users' => 'Users',
            'channels' => 'Channels',
        ],
        'parent' => 'Parent',
    ],
    'profile' => [
        'usergroups' => [
            'tab' => 'User Groups',
            'groups' => 'Groups',
        ],
    ],
];