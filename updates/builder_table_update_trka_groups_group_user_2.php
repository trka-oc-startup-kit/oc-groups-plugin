<?php namespace trka\Groups\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTrkaGroupsGroupUser2 extends Migration
{
    public function up()
    {
        Schema::table('trka_groups_group_user', function($table)
        {
            $table->string('role', 64)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('trka_groups_group_user', function($table)
        {
            $table->dropColumn('role');
        });
    }
}
