<?php namespace trka\Groups\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1015 extends Migration
{
  public function up()
{
    Schema::table('trka_groups_group_user', function($table)
    {
        $table->dropPrimary(['user_id','group_id']);
        $table->string('role', 64)->nullable(false)->default('trka:groups:group_member')->change();
        $table->primary(['user_id','group_id','role']);
    });
}

public function down()
{
    Schema::table('trka_groups_group_user', function($table)
    {
        $table->dropPrimary(['user_id','group_id','role']);
        $table->string('role', 64)->nullable()->default('NULL')->change();
        $table->primary(['user_id','group_id']);
    });
}
}