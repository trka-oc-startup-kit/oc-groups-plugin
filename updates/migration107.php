<?php namespace trka\Groups\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration107 extends Migration
{
    public function up()
    {
        Schema::table('trka_groups_groups', function($table)
        {
            $table->longText('homepage_content')->nullable();
            $table->longText('homepage_content_html')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('trka_groups_groups', function($table)
        {
            $table->dropColumn('homepage_content');
            $table->dropColumn('homepage_content_html');
        });
    }
}