<?php namespace trka\Groups\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTrkaGroupsGroupForum extends Migration
{
    public function up()
    {
        Schema::create('trka_groups_group_forum', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('forum_id')->unsigned();
            $table->integer('group_id')->unsigned();
            $table->primary(['forum_id','group_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('trka_groups_group_forum');
    }
}
