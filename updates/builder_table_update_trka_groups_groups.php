<?php namespace trka\Groups\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTrkaGroupsGroups extends Migration
{
    public function up()
    {
        Schema::table('trka_groups_groups', function($table)
        {
            $table->integer('forum_channel_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('trka_groups_groups', function($table)
        {
            $table->dropColumn('forum_channel_id');
        });
    }
}
