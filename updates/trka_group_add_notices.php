<?php namespace trka\Groups\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class trka_group_add_notices extends Migration
{
    public function up()
    {
        Schema::table('trka_groups_groups', function($table)
        {
            $table->text('notices')->nullable();
            $table->text('sidebar_slots')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('trka_groups_groups', function($table)
        {
            $table->dropColumn([
                'notices',
                'sidebar_slots'
            ]);
        });
    }
}