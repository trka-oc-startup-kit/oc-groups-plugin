<?php namespace trka\Groups\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTrkaGroupsGroups3 extends Migration
{
    public function up()
    {
        Schema::table('trka_groups_groups', function($table)
        {
            $table->string('group_icon', 255);
        });
    }
    
    public function down()
    {
        Schema::table('trka_groups_groups', function($table)
        {
            $table->dropColumn('group_icon');
        });
    }
}
