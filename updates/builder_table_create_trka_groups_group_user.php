<?php namespace trka\Groups\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTrkaGroupsGroupUser extends Migration
{
    public function up()
    {
        Schema::create('trka_groups_group_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('user_id')->unsigned();
            $table->integer('group_id')->unsigned();
            $table->primary(['user_id','group_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('trka_groups_group_user');
    }
}
