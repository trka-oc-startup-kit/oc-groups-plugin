<?php namespace trka\Groups\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTrkaGroupsGroupUser extends Migration
{
    public function up()
    {
        Schema::table('trka_groups_group_user', function($table)
        {
            $table->integer('moderator')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('trka_groups_group_user', function($table)
        {
            $table->dropColumn('moderator');
        });
    }
}
