<?php namespace trka\Groups;

use Backend\Models\User;
use System\Classes\PluginBase;
use RainLab\User\Models\User as UserModel;
use RainLab\User\Controllers\Users as UsersController;
use trka\Groups\Models\Group;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\trka\Groups\Components\Usergroup' => 'usergroup',
            '\trka\Groups\Components\Usergroups' => 'usergroups',
        ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        UserModel::extend(function ($model) {
            // @todo: in Group, I deprecated belongsToMany in favor of a single belongsTo. Should that be reflected in usermodel also?
            $model->belongsToMany['usergroups'] = [
                'trka\Groups\Models\Group',
                'table' => 'trka_groups_group_user',
                'key' => 'user_id',
                'otherKey' => 'group_id',
                'pivot' => ['role', 'moderator']
            ];
            $model->addDynamicMethod('isInGroup', function ($group) use ($model) {
                if ($group == null) {
                    return false;
                }
                return $model->usergroups->contains($group->id);
            });
            $model->addDynamicMethod('getGroupRoles', function ($group) use ($model) {
                if ($group == null) {
                    return false;
                }
                return explode('|', $model->usergroups->where('id', $group->id)->first()->pivot->role);
            });
            $model->addDynamicMethod('hasGroupRole', function ($group, $role) use ($model) {
                if ($group == null) {
                    return false;
                }
                $is = $group->usersByRole($role)->contains('id', $model->id);
                return $is;
            });
        });

        UsersController::extend(function ($controller) {
            if (!isset($controller->implement['Backend.Behaviors.RelationController'])) {
                $controller->implement[] = 'Backend.Behaviors.RelationController';
            }
            $controller->relationConfig = '$/trka/groups/controllers/userprofile/config_relations.yml';
        });

        UsersController::extendFormFields(function ($form, $model, $context) {
            if (!$model instanceof UserModel) {
                return;
            }
            if (!$model->exists) {
                return;
            }

            $form->addTabFields([
                'usergroups' => [
                    'tab' => 'trka.groups::lang.profile.usergroups.tab',
                    'label' => 'trka.groups::lang.profile.usergroups.groups',
                    'type' => 'relation',
                    'nameFrom' => 'name',
                ]
            ]);
        });
    }
}
